import { Site } from 'src/endpoints/sites/entities/site.entity';
import { AfterLoad, Column, CreateDateColumn, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity('checks')
export class Check {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    up?: boolean;

    @Column()
    latency?: number;

    @Column("int", { nullable: true, select : false})
    siteId?: number;

    @Column("int", { nullable: true, })
    code? : number;


    @ManyToOne(() => Site, (site: Site) => site.checks)
    site?: Site;
    
    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)"})
    public created_at?: Date;
}