import { Module } from '@nestjs/common';
import { ChecksService } from './checks.service';
import { ChecksController } from './checks.controller';
import { CachingModule } from 'src/common/caching/caching.module';
import { DatabaseModule } from 'src/common/database/database.module';
import { ElasticsearchModule, ElasticsearchService } from '@nestjs/elasticsearch';

@Module({
  imports : [DatabaseModule, CachingModule],
  controllers: [ChecksController],
  providers: [ChecksService],
  exports : [ChecksService]
})
export class ChecksModule {}
