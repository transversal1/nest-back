import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Check } from './entities/check.entity';

@Injectable()
export class ChecksService {

    constructor(@InjectRepository(Check) private readonly checkRepository: Repository<Check>,
    private elasticSearchService : ElasticsearchService) { }


    create(check: any) {
        // return this.elasticSearchService.index({
        //     index : `pings_${check.siteId}`,
        //     document : check
        // })
        return this.checkRepository.save(check);
    }


    async search(site_id : number) : Promise<Check[]>{
        // const res =  await  this.elasticSearchService.search({
        //     index : `pings_${site_id}`
        // })
        // return res.hits.hits.map(h => h._source) as any
        return await this.checkRepository.createQueryBuilder('checks').where('SiteId = :id', {id:site_id}).getMany()
    }
}
