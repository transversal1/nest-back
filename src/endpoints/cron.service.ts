import { HttpException, Inject, Injectable, Logger, OnApplicationBootstrap, OnModuleInit } from "@nestjs/common";
import { SchedulerRegistry } from "@nestjs/schedule";
import { Site } from "src/endpoints/sites/entities/site.entity";
import { HttpService } from "@nestjs/axios";
import { firstValueFrom, Observable } from "rxjs";
import { SitesService } from "src/endpoints/sites/sites.service";
import { ChecksService } from "src/endpoints/checks/checks.service";
import { AxiosError } from "axios";
import { ApiConfigService } from "src/common/api-config/api.config.service";

@Injectable()
export class CronService implements OnApplicationBootstrap {
  constructor(
    private readonly http: HttpService,
    private siteService: SitesService,
    private apiConfigService: ApiConfigService,
    private checkService: ChecksService,
    private readonly schedulerRegistry: SchedulerRegistry,
  ) { }
  onApplicationBootstrap() {
    this.siteService.getAll().then(sites => {
      sites.forEach(site => {
        this.registerCheck(site);
      });
    });
  }


  public registerCheck(site: Site) {
    if (this.apiConfigService.getIsCacheWarmerFeatureActive()) {

      const interval: any = setInterval(async () => {
        let req: Observable<any>;
        const start = new Date();
        switch (site.method) {
          case "GET":
            req = this.http.get(site.url!)
            break;
          case "POST":
            req = this.http.post(site.url!)
            break;
          case "HEAD":
            req = this.http.head(site.url!)
            break;
          case "DELETE":
            req = this.http.delete(site.url!)
            break;
          case "PUT":
            req = this.http.put(site.url!)
            break;
          case "PATCH":
            req = this.http.patch(site.url!)
            break;
        }
        let response;

        try {
          response = await firstValueFrom(req!);
          const end = new Date()
          const check = await this.checkService.create({
            latency: end.getTime() - start.getTime(),
            up: this.inRange(response.status, 200, 299),
            siteId: site.id,
            code: response.status,
            created_at: end
          });
        } catch (e: any) {
          const error: AxiosError = e;
          const end = new Date()
          await this.checkService.create({
            latency: end.getTime() - start.getTime(),
            up: false,
            code: error.code || null,
            siteId: site.id,
            message: error.message,
            created_at: end
          });
          //   //TODO : send notification
        }

      }, site.interval! * 1000)
      this.schedulerRegistry.addInterval(site.id!.toString(), interval)
    }
  }


  private inRange(x: number, min: number, max: number) {
    return ((x - min) * (x - max) <= 0);
  }
}