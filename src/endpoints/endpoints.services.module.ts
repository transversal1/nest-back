import { forwardRef, Module } from "@nestjs/common";
import { UsersModule } from "./users/user.module";
import { ApiConfigModule } from "src/common/api-config/api.config.module";
import { JwtModule } from "@nestjs/jwt";
import { ApiConfigService } from "src/common/api-config/api.config.service";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { JwtStrategy } from "src/utils/guards/jwt.strategy";
import { PassportModule } from "@nestjs/passport";
import { UsersService } from "./users/user.service";
import { SitesModule } from './sites/sites.module';
import { ChecksModule } from './checks/checks.module';
import { CronModule } from "src/crons/cron.module";
import { DockerModule } from './docker/docker.module';

@Module({
  imports: [
    UsersModule,
    ConfigModule,
    ApiConfigModule,
    SitesModule,
    ChecksModule,
    // forwardRef(()=>CronModule),
    DockerModule
  ],
  exports: [
    UsersModule,
    SitesModule,
    ChecksModule
  ],
  providers: [ConfigService,
    {
      provide: 'JWT_STRATEGY',
      useFactory: (apiConfigService: ApiConfigService, userService : UsersService) => {
        return new JwtStrategy(apiConfigService, userService);
      },
      inject: [ApiConfigService, UsersService] 
    }
  ],
})
export class EndpointsServicesModule { }