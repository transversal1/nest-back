import { Module } from '@nestjs/common';
import { SitesService } from './sites.service';
import { SitesController } from './sites.controller';
import { DatabaseModule } from 'src/common/database/database.module';
import { CachingModule } from 'src/common/caching/caching.module';
import { ChecksModule } from '../checks/checks.module';

@Module({
  imports : [DatabaseModule, CachingModule, ChecksModule],
  controllers: [SitesController],
  providers: [SitesService],
  exports : [SitesService]
})
export class SitesModule {}
