import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ChecksService } from '../checks/checks.service';
import { Site } from './entities/site.entity';

@Injectable()
export class SitesService {

    constructor(@InjectRepository(Site) private readonly siteRepository: Repository<Site>,
        private checkService: ChecksService) { }

    async getAll() {
        let sites = await this.siteRepository.createQueryBuilder('sites').getMany();
        return await Promise.all(
            sites.flatMap(async (site) => {
                try {
                    site.checks = await this.checkService.search(site.id!);
                    site.disponibility = parseFloat((site!.checks!.filter(c => c.up).length / (site!.checks!.length || 1) * 100).toFixed(3));
                } catch (err) {
                    console.log(err);
                    site.checks = [];
                    site.disponibility = 100
                }
                return site;
            })
        )
    }

    async find(id: number) {
        const site = await this.siteRepository.createQueryBuilder('sites')
            .where('sites.id = :id', { id })
            .leftJoinAndSelect('sites.checks', 'site_id')
            .getOne();

        site!.disponibility = site!.checks!.filter(c => c.up).length / (site!.checks!.length || 1) * 100;
        return site;
    }

    async create(site: Site) {
        return this.siteRepository.save(site);
    }


    async delete(id: number) {
        return this.siteRepository.createQueryBuilder('sites').delete().from(Site).where('id = :id', { id }).execute();
    }

}
