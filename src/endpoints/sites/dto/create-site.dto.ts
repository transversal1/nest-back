import { PartialType } from '@nestjs/mapped-types';

export class CreateSiteDto {
    name? : string;
    interval? : number;
    url? : string;
    method? : string;

}
