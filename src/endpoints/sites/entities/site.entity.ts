import { Check } from 'src/endpoints/checks/entities/check.entity';
import { AfterLoad, Column, CreateDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity('sites')
export class Site {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  name?: string;

  @Column()
  url?: string;

 
  @Column()
  interval?: number;

  @Column()
  method?: string;

  @OneToMany(() => Check, (check: Check) => check.site, {cascade :true})
  checks?: Check[];

  disponibility? : number;
  
  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" , select: false } )
  public created_at?: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)", select: false  })
  public updated_at?: Date;
}