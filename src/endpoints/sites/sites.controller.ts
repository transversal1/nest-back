import { Body, Controller, Delete, Get, Param, Post, Request } from '@nestjs/common';
import { ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateSiteDto } from './dto/create-site.dto';
import { Site } from './entities/site.entity';
import { SitesService } from './sites.service';

@Controller('sites')
@ApiTags('sites')
export class SitesController {
  constructor(private readonly sitesService: SitesService) { }

  @Get("/")
  @ApiResponse({
    status: 200,
    description: 'Get all sites',
  })
  async get() {
    return await this.sitesService.getAll();
  }

  @Get("/:id")
  @ApiResponse({
    status: 200,
    description: 'Get one site',
  })
  async find(@Param('id') id : number) {
    return await this.sitesService.find(id);
  }

  @Post("/")
	@ApiResponse({
		status: 200,
		description: 'Create site',
	})
  @ApiParam(CreateSiteDto)
  create(@Body() site : CreateSiteDto){
    return this.sitesService.create(site as Site);
  }


  @Delete("/:id")
	@ApiResponse({
		status: 200,
		description: 'Delete site',
	})
  delete(@Param('id') id : any){
    return this.sitesService.delete(id);
  }

}
