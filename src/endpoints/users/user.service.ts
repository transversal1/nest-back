import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { UsersListType } from 'aws-sdk/clients/cognitoidentityserviceprovider';
import { CachingService } from 'src/common/caching/caching.service';
import { Constants } from 'src/utils/constants';
import { In, Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  private client: CognitoIdentityServiceProvider;
  constructor(
    private configService: ConfigService
  ) {
    this.client = new CognitoIdentityServiceProvider({
      region: "eu-west-2",
      credentials: {
        accessKeyId: "AKIASJWOHPU3BJBQJBN4",
        secretAccessKey: "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"
      }
    });

  }

  async list(): Promise<UsersListType | undefined> {
    return (await this.client.listUsers({ UserPoolId: "eu-west-2_xpVpljinf" }).promise()).Users
  }

  async create(user: any) {
    return await this.client.adminCreateUser({
      Username: user.username,
      UserPoolId: "eu-west-2_xpVpljinf",
      UserAttributes: [
        {
          Name: "email",
          Value: user.email
        }
      ]
    }).promise()
  }


  async enable(username: string) {
    return await this.client.adminEnableUser({
      Username: username,
      UserPoolId: "eu-west-2_xpVpljinf",
    }).promise();
  }

  async disable(username: string) {
    return await this.client.adminDisableUser({
      Username: username,
      UserPoolId: "eu-west-2_xpVpljinf",
    }).promise();
  }


  async delete(username : string){
    return await this.client.adminDeleteUser({
      Username : username,
      UserPoolId : "eu-west-2_xpVpljinf"
    }).promise()
  }
}