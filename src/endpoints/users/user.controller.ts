import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { User } from './entities/user.entity';
import { UsersService } from './user.service';
import { CognitoIdentityServiceProvider } from 'aws-sdk'
@Controller('users')
@ApiTags('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }


  @Get('/')
  async list() {
   return await this.usersService.list()
  }


  @Post('/')
  async create(@Body() req : any) {
   return await this.usersService.create(req)
  }

  @Put('/enable/:username')
  async enable(@Param('username') username : any) {
   return await this.usersService.enable(username)
  }

  @Put('/disable/:username')
  async disable(@Param('username') username : any) {
   return await this.usersService.disable(username)
  }


  @Delete("/:username")
  async delete(@Param('username') username : any) {
    return await this.usersService.delete(username)
   }
}