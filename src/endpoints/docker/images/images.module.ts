import { Module } from '@nestjs/common';
import { ImagesService } from './images.service';
import { ImagesController } from './images.controller';
import { EndpointsService } from '../endpoints/endpoints.service';
import { EndpointsModule } from '../endpoints/endpoints.module';
import { DockerService } from '../docker.service';
import { CryptoService } from 'src/utils/services/crypto.service';
import { S3Service } from 'src/utils/services/s3.service';
import { ConfigModule } from '@nestjs/config';
import { ContainersModule } from '../containers/containers.module';

@Module({
  controllers: [ImagesController],
  providers: [ImagesService, DockerService, CryptoService, S3Service],
  imports : [EndpointsModule, ConfigModule, ContainersModule]
})
export class ImagesModule {}
