import { Controller, Get, Post, Body, Patch, Param, Delete, Sse, Query } from '@nestjs/common';
import { ImagesService } from './images.service';
import { CreateImageDto } from './dto/create-image.dto';
import { UpdateImageDto } from './dto/update-image.dto';

@Controller('images')
export class ImagesController {
  constructor(private readonly imagesService: ImagesService) {}


  @Get("/:endpoint_id")
  findAll(@Param("endpoint_id") endpoint_id : number) {
    return this.imagesService.findAll(endpoint_id);
  }

  @Sse('/sse/:endpoint_id/pull')
  pull(@Param("endpoint_id") endpoint_id : number, @Query() query : any) {
    return this.imagesService.pull(endpoint_id, query.image);
  }

  @Get('/:endpoint_id/:image_id/download')
  download(@Param("endpoint_id") endpoint_id : number, @Param("image_id") image_id : string){
    return this.imagesService.download(endpoint_id, image_id);
  }


  @Delete('/:endpoint_id/:id')
  remove(@Param("endpoint_id") endpoint_id : number,@Param('id') id: string) {
    return this.imagesService.remove(endpoint_id, id);
  }
}
