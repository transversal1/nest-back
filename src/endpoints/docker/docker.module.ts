import { Module } from '@nestjs/common';
import { DockerService } from './docker.service';
import { EndpointsModule } from './endpoints/endpoints.module';
import { ContainersModule } from './containers/containers.module';
import { DatabaseModule } from 'src/common/database/database.module';
import { CachingModule } from 'src/common/caching/caching.module';
import { ImagesModule } from './images/images.module';
import { CryptoService } from 'src/utils/services/crypto.service';
import { ConfigModule } from '@nestjs/config';
import { S3Service } from 'src/utils/services/s3.service';
import { NetworksModule } from './networks/networks.module';

@Module({
  controllers: [],
  providers: [DockerService, CryptoService, S3Service],
  imports: [EndpointsModule, ContainersModule,ConfigModule, DatabaseModule, CachingModule, ImagesModule, NetworksModule],
  exports : [DockerService]
})
export class DockerModule {}
