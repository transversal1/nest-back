import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { VolumesService } from './volumes.service';

@Controller('volumes')
@ApiTags('volumes')
export class VolumesController {
  constructor(private readonly volumesService: VolumesService) {}

  @Get()
  findAll() {
    return this.volumesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.volumesService.findOne(+id);
  }


  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.volumesService.remove(+id);
  }
}
