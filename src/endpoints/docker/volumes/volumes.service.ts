import { Injectable } from '@nestjs/common';

@Injectable()
export class VolumesService {

  findAll() {
    return `This action returns all volumes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} volume`;
  }

  remove(id: number) {
    return `This action removes a #${id} volume`;
  }
}
