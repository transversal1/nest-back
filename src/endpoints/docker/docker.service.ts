import { Injectable } from '@nestjs/common';
import { CryptoService } from 'src/utils/services/crypto.service';
import { EndpointsService } from './endpoints/endpoints.service';
import { Endpoint } from './endpoints/entities/endpoint.entity';
import {Docker} from 'node-docker-api';
import * as url from 'url'
import { S3Service } from 'src/utils/services/s3.service';
@Injectable()
export class DockerService {

  clients = new Map()
  constructor(private cryptoService : CryptoService,
    private s3Service : S3Service) { }

  async info(endpoint: Endpoint) {
    const docker = await this.createClient(endpoint!);
    return await docker.info()
  }

  async ping(endpoint: Endpoint): Promise<String> {
    const docker = await this.createClient(endpoint!);
    return await docker.ping()
  }


  public async createClient(endpoint: Endpoint) {
    let docker;
    if(this.clients.has(endpoint.id)){
      return this.clients.get(endpoint.id);
    }
    switch (endpoint.protocol) {
      case "socket":
        docker = new Docker({ socketPath: endpoint.url });
        break;

      case "http":
        if (endpoint.tls){
          let options : any = { protocol:'https', host: endpoint.url, port: endpoint.port}
          if (endpoint.ca_certificat){
            options.ca = (await this.s3Service.getFileContent(endpoint.ca_certificat))?.toString();
          }
          if(endpoint.tls_certificat){
            options.cert = (await this.s3Service.getFileContent(endpoint.tls_certificat))?.toString();
          }
          if(endpoint.tls_key){
            options.key = (await this.s3Service.getFileContent(endpoint.tls_key))?.toString();
          }
          console.log(options);
          docker = new Docker({ protocol:'https', host: endpoint.url, port: endpoint.port, ca: endpoint.ca_certificat })
        }
        else
          docker = new Docker({ host: endpoint.url, port: endpoint.port });
        break;

      case "ssh":
        if(endpoint.password){
          const uri = new url.URL(endpoint.url!); 
          const password = this.cryptoService.decrypt(endpoint.password);
          const user = uri.username
          endpoint.url = uri.href;
          // privateKey?: Buffer | string;
          /** For an encrypted private key, this is the passphrase used to decrypt it. */
          // passphrase?: Buffer | string;
          docker = new Docker({ prococol: "ssh", host: uri.hostname, port: endpoint.port, password, username: user, sshOptions : {username : user, password, host: uri.hostname, port: endpoint.port, tryKeyboard : true} });
          docker.modem.protocol = "ssh"
        }else{
          docker = new Docker({ prococol: "ssh", host: endpoint.url, port: endpoint.port });
        }
        break;

      default:
        docker = new Docker({ socketPath: '/var/run/docker.sock' });
        break;
    }
    this.clients.set(endpoint.id, docker);
    return docker;
  }

}
