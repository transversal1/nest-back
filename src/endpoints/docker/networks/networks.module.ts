import { Module } from '@nestjs/common';
import { NetworksService } from './networks.service';
import { NetworksController } from './networks.controller';
import { EndpointsService } from '../endpoints/endpoints.service';
import { EndpointsModule } from '../endpoints/endpoints.module';
import { DockerService } from '../docker.service';
import { CryptoService } from 'src/utils/services/crypto.service';
import { S3Service } from 'src/utils/services/s3.service';
import { ConfigModule } from '@nestjs/config';
import { ContainersModule } from '../containers/containers.module';

@Module({
  controllers: [NetworksController],
  providers: [NetworksService, DockerService, CryptoService, S3Service],
  imports : [EndpointsModule, ConfigModule, ContainersModule]
})
export class NetworksModule {}
