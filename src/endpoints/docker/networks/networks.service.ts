import { Injectable, StreamableFile } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ContainersService } from '../containers/containers.service';
import { DockerService } from '../docker.service';
import { EndpointsService } from '../endpoints/endpoints.service';
import { Endpoint } from '../endpoints/entities/endpoint.entity';

@Injectable()
export class NetworksService {
  constructor(private endpointService: EndpointsService,
    private containerService : ContainersService,
    private dockerService: DockerService) { }

  async findAll(endpoint_id: number) {
    const endpoint = await this.endpointService.findOne(endpoint_id);
    const docker = await this.dockerService.createClient(endpoint!);
    const networks_ids = (await this.containerService.findAll(endpoint_id)).map((c:any) => {
      const network_key = Object.keys(c.NetworkSettings.Networks)[0];
      return c.NetworkSettings.Networks[network_key].NetworkID;
    })
    const networks = new Set(networks_ids);
    console.log(networks)
    return docker.network.list({all : true}).then((list: any) => {
      // return list.map((network: any) => network.data);
      return list.map((network: any) =>  Object.assign(network.data, {used: networks.has(network.data.Id)}));
    })
  }

  private async findOne(endpoint_id: number, id : string) {
    const endpoint = await this.endpointService.findOne(endpoint_id);
    const docker = await this.dockerService.createClient(endpoint!);
    return (await docker.image.list({all : true})).find((image:any) => image.data.Id == id)
  }



  async pull(endpoint_id : number, image_name : string){
    const endpoint = await this.endpointService.findOne(endpoint_id);
    const docker = await this.dockerService.createClient(endpoint!);
    const stream = await docker.image.create({}, { fromImage: image_name.includes(':') ? image_name : image_name+':latest' })
    return fromStream(stream);
  }


  async download(endpoint_id : number, image_id : string){
    const endpoint = await this.endpointService.findOne(endpoint_id);
    const docker = await this.dockerService.createClient(endpoint!);
    const stream = await docker.image.getAll({name: image_id});
    return new StreamableFile(stream);
  }




  async remove(endpoint_id: number, id : string) {
    const image = await this.findOne(endpoint_id, id);
    return await image.remove()
  }
} 


export default function fromStream(stream : any, finishEventName = 'end', dataEventName = 'data') {
  stream.pause();
  return new Observable<any>((observer) => {
    function dataHandler(data : any) {
      observer.next(data.toString());
    }

    function errorHandler(err : any) {
      observer.error(err);
    }

    function endHandler() {
      observer.complete();
    }

    stream.addListener(dataEventName, dataHandler);
    stream.addListener('error', errorHandler);
    stream.addListener(finishEventName, endHandler);

    stream.resume();

    return () => {
      stream.removeListener(dataEventName, dataHandler);
      stream.removeListener('error', errorHandler);
      stream.removeListener(finishEventName, endHandler);
    };
  });
}
