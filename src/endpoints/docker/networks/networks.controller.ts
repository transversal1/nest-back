import { Controller, Get, Post, Body, Patch, Param, Delete, Sse, Query } from '@nestjs/common';
import { NetworksService } from './networks.service';

@Controller('networks')
export class NetworksController {
  constructor(private readonly networksService: NetworksService) {}


  @Get("/:endpoint_id")
  findAll(@Param("endpoint_id") endpoint_id : number) {
    return this.networksService.findAll(endpoint_id);
  }

  @Sse('/sse/:endpoint_id/pull')
  pull(@Param("endpoint_id") endpoint_id : number, @Query() query : any) {
    return this.networksService.pull(endpoint_id, query.image);
  }

  @Get('/:endpoint_id/:image_id/download')
  download(@Param("endpoint_id") endpoint_id : number, @Param("image_id") image_id : string){
    return this.networksService.download(endpoint_id, image_id);
  }


  @Delete('/:endpoint_id/:id')
  remove(@Param("endpoint_id") endpoint_id : number,@Param('id') id: string) {
    return this.networksService.remove(endpoint_id, id);
  }
}
