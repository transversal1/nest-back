import { forwardRef, Module } from '@nestjs/common';
import { EndpointsService } from './endpoints.service';
import { EndpointsController } from './endpoints.controller';
import { DatabaseModule } from 'src/common/database/database.module';
import { CachingModule } from 'src/common/caching/caching.module';
import { DockerModule } from '../docker.module';
import { DockerService } from '../docker.service';
import { CryptoService } from 'src/utils/services/crypto.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { S3Service } from 'src/utils/services/s3.service';

@Module({
  imports : [DatabaseModule, CachingModule, forwardRef(() => DockerModule), ConfigModule],
  controllers: [EndpointsController],
  providers: [EndpointsService, DockerService, CryptoService, S3Service],
  exports : [EndpointsService]
})
export class EndpointsModule {}
