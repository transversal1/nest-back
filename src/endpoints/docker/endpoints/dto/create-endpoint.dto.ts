export class CreateEndpointDto {
    name?: string;

    protocol? : string

    url?: string;

    tls?: boolean;

    ca_certificat?: string;

    tls_certificat?: string;

    tls_key?: string;
    
    password? : string;
}
