import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CryptoService } from 'src/utils/services/crypto.service';
import { S3Service } from 'src/utils/services/s3.service';
import { Repository } from 'typeorm';
import { DockerService } from '../docker.service';
import { CreateEndpointDto } from './dto/create-endpoint.dto';
import { UpdateEndpointDto } from './dto/update-endpoint.dto';
import { Endpoint } from './entities/endpoint.entity';

@Injectable()
export class EndpointsService {

  constructor(@InjectRepository(Endpoint) private readonly endpointRepository: Repository<Endpoint>,
    private cryptoService: CryptoService,
    private s3Service: S3Service,
    private dockerService: DockerService) { }


  async create(endpoint: CreateEndpointDto, files: Array<Express.Multer.File>) {

    for (let file of files) {
      endpoint.tls = true;
      //@ts-ignore
      endpoint[file.fieldname] = await this.s3Service.uploadFile(file!, endpoint.name!) as string;
    }
    if (endpoint.password) {
      endpoint.password = this.cryptoService.encrypt(endpoint.password)
    }
    return this.endpointRepository.save(endpoint);
  }

  async findAll() {
    const endpoints = await this.endpointRepository.createQueryBuilder('endpoints').getMany();
    return await Promise.all(
      endpoints.flatMap(async (e) => {
        try {
          e.status = (await this.dockerService.ping(e)).toString();
          e.info = await this.dockerService.info(e) || null;
        } catch (err) {
          console.log(err);
          e.status = "DOWN"
          e.info = null;
        }
        return e;
      })
    )
  }


  getEndpointPassword(id: number) {
    return this.endpointRepository.createQueryBuilder('endpoints').select('password').where("id = :id", { id }).getOne()
  }

  findOne(id: number) {
    return this.endpointRepository.createQueryBuilder('endpoints')
      .where('id = :id', { id }).getOne();
  }

  update(id: number, updateEndpointDto: UpdateEndpointDto) {
    return `This action updates a #${id} endpoint`;
  }

  remove(id: number) {
    return this.endpointRepository.createQueryBuilder('endpoints').delete().from(Endpoint).where('id = :id', { id }).execute();
  }
}
