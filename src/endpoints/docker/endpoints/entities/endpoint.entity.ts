import { createCipheriv } from "crypto";
import { BeforeInsert, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('endpoints')
export class Endpoint {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    name?: string;

    @Column()
    protocol? : string | "socket" | "http" | "ssh"
    
    @Column({select : true, nullable : true})
    password? : string 
    
    @Column({nullable : true})
    url?: string;

    @Column({nullable : true})
    port? : number;

    @Column({ default: false })
    tls?: boolean;

    @Column({ nullable: true })
    ca_certificat?: string;

    @Column({ nullable: true })
    tls_certificat?: string;

    @Column({ nullable: true })
    tls_key?: string;

    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", select: false })
    public created_at?: Date;

    @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)", select: false })
    public updated_at?: Date;


    info?: any;
    status?:any;
}
