import { Controller, Get, Post, Body, Patch, Param, Delete, Put, Header, Sse } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { ContainersService } from './containers.service';
import { CreateContainerDto } from './dto/create-container.dto';
import { UpdateContainerDto } from './dto/update-container.dto';

@Controller('containers')
@ApiTags('containers')
export class ContainersController {
  constructor(private readonly containersService: ContainersService) {}

  @Post('/:endpoint_id')
  create(@Param('endpoint_id') endpoint_id : number, @Body() createContainerDto: CreateContainerDto) {
    return this.containersService.create(endpoint_id, createContainerDto);
  }

  @Get('/:endpoint_id')
  findAll(@Param('endpoint_id') endpoint_id : number) {
    return this.containersService.findAll(endpoint_id);
  }

  @Get('/:endpoint_id/:container_id')
  find(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string){
    return this.containersService.find(endpoint_id, container_id);
  }

  @Post('/:endpoint_id/:container_id/start')
  @ApiResponse({
    status: 200,
    description: 'Start the container with containter_id on endpoint',
  })
  start(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string){
    return this.containersService.start(endpoint_id, container_id);
  }


  @Post('/:endpoint_id/:container_id/stop')
  @ApiResponse({
    status: 200,
    description: 'Stop the container with containter_id on endpoint',
  })
  stop(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string){
    return this.containersService.stop(endpoint_id, container_id);
  }

  @Post('/:endpoint_id/:container_id/restart')
  @ApiResponse({
    status: 200,
    description: 'Restart the container with containter_id on endpoint',
  })
  restart(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string){
    return this.containersService.restart(endpoint_id, container_id);
  }


  @Put('/:endpoint_id/:container_id/rename')
  @ApiResponse({
    status: 200,
    description: 'Rename the container with containter_id on endpoint',
  })
  rename(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string, @Body() req : any){
    return this.containersService.rename(endpoint_id, container_id, req.name);
  }


  @Get('/:endpoint_id/:container_id/logs')
  @Header('content-type', 'application/json')
  logs(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string) {
    return this.containersService.log(endpoint_id, container_id);
  }

  @Sse('/sse/:endpoint_id/:container_id/logs')
  async logs_sse(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string){
    return await this.containersService.log(endpoint_id, container_id, true);
  }

  @Sse('/sse/:endpoint_id/:container_id/stats')
  async stats_sse(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string){
    return await this.containersService.stats(endpoint_id, container_id);
  }

  @Delete('/:endpoint_id/:container_id')
  remove(@Param('endpoint_id') endpoint_id : number, @Param('container_id') container_id : string, @Body() body : any) {
    return this.containersService.remove(endpoint_id, container_id, body.force || false);
  }
}
