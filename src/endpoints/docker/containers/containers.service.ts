import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AnyARecord } from 'dns';
import { firstValueFrom, Observable } from 'rxjs';
import { Stream } from 'stream';
import { DockerService } from '../docker.service';
import { EndpointsService } from '../endpoints/endpoints.service';
import { Endpoint } from '../endpoints/entities/endpoint.entity';
import { CreateContainerDto } from './dto/create-container.dto';
import { UpdateContainerDto } from './dto/update-container.dto';
const { Docker } = require('node-docker-api');
const toArray = require('stream-to-array')
@Injectable()
export class ContainersService {
  constructor(private dockerService: DockerService,
    private endpointService: EndpointsService) { }



  
  async create(endpoint_id: number, createContainerDto : CreateContainerDto) {
    const endpoint = await this.endpointService.findOne(endpoint_id);
    const docker = await this.dockerService.createClient(endpoint!);
    return docker.container.create(createContainerDto).then((container:any) => container.start());
  }

  async findAll(endpoint_id: number) {
    const endpoint = await this.endpointService.findOne(endpoint_id);
    const docker = await this.dockerService.createClient(endpoint!);
    return docker.container.list({all : true}).then((list: any) => {
      return list.map((c: any) => {c.data.Name = c.data.Names[0].substring(1); return c.data});
    })
  }

  async find(endpoint_id: number, container_id: string){
    const endpoint = await this.endpointService.findOne(endpoint_id);
    const docker = await this.dockerService.createClient(endpoint!);
    const container = await docker.container.list({all : true}).then((containers: any) => containers.find((c: any) => c.data.Id == container_id));
    return container.data;
  }


  private async findOne(endpoint_id: number, container_id: string) {
    const endpoint = await this.endpointService.findOne(endpoint_id);
    const docker = await this.dockerService.createClient(endpoint!);
    const container = await docker.container.list({all : true}).then((containers: any) => containers.find((c: any) => c.data.Id == container_id));
    return container
  }


  async stop(endpoint_id: number, container_id: string, force: boolean = false) {
    const container = await this.findOne(endpoint_id, container_id)
    await container.stop()
    return (await container.status()).data
  }

  async start(endpoint_id: number, container_id: string) {
    const container = await this.findOne(endpoint_id, container_id)
    await container.start()
    return (await container.status()).data
  }

  async restart(endpoint_id: number, container_id: string) {
    const container = await this.findOne(endpoint_id, container_id)
    await container.restart()
    return (await container.status()).data
  }

  async rename(endpoint_id: number, container_id: string, name: string) {
    const container = await this.findOne(endpoint_id, container_id);
    await container.rename({ name: name })
    return (await container.status()).data;
  }


  async log(endpoint_id: number, container_id: string, sse : boolean = false) : Promise<Observable<any> | string> {
    const container = await this.findOne(endpoint_id, container_id);
    if(sse){
      const stream = await container.logs({ stdout: 1, follow: 1, stderr: 1,since : Math.round(new Date().getTime()/1000) });
      return fromStream(stream);
    }
    const logs = await new Promise(async (resolve, reject) => {
      const logs = await container.logs({ stdout: 1, follow: false, stderr: true }).then((stream: any) => {
        let chunks: any = [];
        stream.on('data', (chunk: any) => chunks.push(Buffer.from(chunk)));
        stream.on('error', (err: any) => err);
        stream.on('end', () => {
          resolve(Buffer.concat(chunks).toString())
        });
      })
    })
    return JSON.stringify(logs as string)
  }


  async stats(endpoint_id: number, container_id: string) : Promise<Observable<any> | string> {
    const container = await this.findOne(endpoint_id, container_id);
      const stream = await container.stats();
      return fromStream(stream);
  }





  async attach(endpoint_id: number, container_id: string){
    const container = await this.findOne(endpoint_id, container_id);
    return await container.attach({
      stream: true,
      stdout: true,
      stderr: true,
      stdin : true
    })
  }

  async exec(endpoint_id: number, container_id: string, command : string){
    const container = await this.findOne(endpoint_id, container_id);
    const exec =  await container.exec.create({
      AttachStdout: true,
      AttachStderr: true,
      AttachStdin : true,
      Tty : true,
      Cmd: command.split(" ")
    })
    return exec.start();
  }

  async remove(endpoint_id: number, container_id: string, force: boolean = false) {
    const container = await this.findOne(endpoint_id, container_id);
    return await container.delete({ force })
  }

  
}

export default function fromStream(stream : any, finishEventName = 'end', dataEventName = 'data') {
  stream.pause();
  return new Observable<any>((observer) => {
    function dataHandler(data : any) {
      observer.next(data.toString());
    }

    function errorHandler(err : any) {
      observer.error(err);
    }

    function endHandler() {
      observer.complete();
    }

    stream.addListener(dataEventName, dataHandler);
    stream.addListener('error', errorHandler);
    stream.addListener(finishEventName, endHandler);

    stream.resume();

    return () => {
      stream.removeListener(dataEventName, dataHandler);
      stream.removeListener('error', errorHandler);
      stream.removeListener(finishEventName, endHandler);
    };
  });
}