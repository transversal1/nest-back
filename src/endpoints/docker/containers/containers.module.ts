import { Module } from '@nestjs/common';
import { ContainersService } from './containers.service';
import { ContainersController } from './containers.controller';
import { HttpModule } from '@nestjs/axios';
import { EndpointsModule } from '../endpoints/endpoints.module';
import { EndpointsService } from '../endpoints/endpoints.service';
import { DatabaseModule } from 'src/common/database/database.module';
import { DockerService } from '../docker.service';
import { CryptoService } from 'src/utils/services/crypto.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { S3Service } from 'src/utils/services/s3.service';

@Module({
  imports : [DatabaseModule,HttpModule, EndpointsModule, ConfigModule],
  controllers: [ContainersController],
  providers: [ContainersService, EndpointsService, DockerService, CryptoService, S3Service],
  exports : [ContainersService]
})
export class ContainersModule {}
