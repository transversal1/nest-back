export class CreateContainerDto {
    Image? : string;
    name? : string;
    MacAddress? : string;
    Env? : string[];
    Cmd? : string | string[];
    Labels? : Object;
    HostConfig? : {
        Memory? : number;
        CpuShares? : number;
        PortBindings? : any;
        NetworkMode? : "bridge" | "host" | "none" | string;
        Dns? : string[]
    }
}
