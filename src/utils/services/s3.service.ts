import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { S3 } from 'aws-sdk';
import { CachingService } from "src/common/caching/caching.service";
@Injectable()
export class S3Service {
    private s3Client;
    public constructor(private configService: ConfigService) {

        this.s3Client = new S3({
            accessKeyId: this.configService.get('s3.accessKey'),
            secretAccessKey: this.configService.get('s3.secretKey'),
            endpoint: this.configService.get('s3.endpoint'),
            s3ForcePathStyle: true,
            signatureVersion: 'v4'
        });
    }


    async getObjectUrl(path: string) {
        if (path) {
            return await this.s3Client.getSignedUrl('getObject', {
                Bucket: "certificats",
                Key: path,
                Expires: 60 * 60
            });
        }
        return undefined;
    }


    async getFileContent(path : string) : Promise<Buffer | undefined>{
        if (path) {
            return (await (await this.s3Client.getObject({Bucket : "certificats", Key : path})).promise()).Body as Buffer
        }
        return undefined;
    }


    async uploadFile(file: Express.Multer.File, endpoint_name : string) {
        const name = "certificats/"+file.fieldname+"/" + endpoint_name + "-" + file.originalname;
        await this.s3Client.putObject({
            Bucket: 'certificats',
            Key: name,
            Body: file.buffer,
            ContentType: file.mimetype,
            ACL: 'public-read',
            // ...((file.base64 || false) && { ContentEncoding: 'base64' }),
        }).promise();
        return name;
    }
}