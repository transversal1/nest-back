import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { createCipheriv, createDecipheriv } from "crypto";

@Injectable()
export class CryptoService {
    key : string = "";
    iv : Buffer;
    constructor(private apiConfig: ConfigService) { 
        this.key = this.apiConfig.get('AESKey')!
        this.iv= Buffer.from('I8zyA4lVhMCaJ5Kg');

    }

    encrypt(password: string) {
        const cipher = createCipheriv('aes-256-cbc', this.key, this.iv);
        return cipher.update(password,'utf-8', 'base64') + cipher.final('base64');
    }



    decrypt(text :string){
        const decipher = createDecipheriv('aes-256-cbc', this.key,this.iv);
        return decipher.update(text, 'base64', 'utf8')+decipher.final('utf8')
    }
}
