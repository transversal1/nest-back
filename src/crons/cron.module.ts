import { HttpModule } from '@nestjs/axios';
import { forwardRef, Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { DatabaseModule } from 'src/common/database/database.module';
import { MicroserviceModule } from 'src/common/microservice/microservice.module';
import { ChecksModule } from 'src/endpoints/checks/checks.module';
import { EndpointsServicesModule } from 'src/endpoints/endpoints.services.module';
import { SitesModule } from 'src/endpoints/sites/sites.module';
import { CommonModule } from '../common/common.module';
import { CronService } from '../endpoints/cron.service';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    forwardRef(() => CommonModule),
    forwardRef(() => EndpointsServicesModule),
    MicroserviceModule,
    DatabaseModule,
    HttpModule,
    SitesModule,
    ChecksModule
  ],
  providers: [
    CronModule,
    CronService
  ],
})
export class CronModule { }
