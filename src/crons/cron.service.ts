import { HttpException, Inject, Injectable, Logger, OnApplicationBootstrap, OnModuleInit } from "@nestjs/common";
import { SchedulerRegistry } from "@nestjs/schedule";
import { Site } from "src/endpoints/sites/entities/site.entity";
import { HttpService } from "@nestjs/axios";
import { firstValueFrom, Observable } from "rxjs";
import { SitesService } from "src/endpoints/sites/sites.service";
import { ChecksService } from "src/endpoints/checks/checks.service";

@Injectable()
export class CronService implements OnApplicationBootstrap {
  constructor(
    private readonly http: HttpService,
    private siteService: SitesService,
    private checkService: ChecksService,
    private readonly schedulerRegistry: SchedulerRegistry,
  ) { }
  onApplicationBootstrap() {
    this.siteService.getAll().then(sites => {
      sites.forEach(site => {
        this.registerCheck(site);
      });
    });
  }


  public registerCheck(site: Site) {
    const interval: any = setInterval(async () => {
      let req: Observable<any>;
      const start = new Date();
      switch (site.method) {
        case "GET":
          req = this.http.get(site.url!)
          break;
        case "POST":
          req = this.http.post(site.url!)
          break;
        case "HEAD":
          req = this.http.head(site.url!)
          break;
        case "DELETE":
          req = this.http.delete(site.url!)
          break;
        case "PUT":
          req = this.http.put(site.url!)
          break;
        case "PATCH":
          req = this.http.patch(site.url!)
          break;
      }
      let response;
      try{

        response = await firstValueFrom(req!);
        const end = new Date()
        const check = await this.checkService.create({
          latency: end.getTime() - start.getTime(),
          up: this.inRange(response.status, 200, 299),
          siteId: site.id,
          code : response.code
        });
      }catch(e : any){
        if(e.response){

          await this.checkService.create({
            latency: 0,
            up: false,
            code : e.response.status || null,
            siteId: site.id
          });
        }
        //   //TODO : send notification
      }
    }, site.interval! * 1000)
    this.schedulerRegistry.addInterval(site.id!.toString(), interval)
  }


  private inRange(x: number, min: number, max: number) {
    return ((x - min) * (x - max) <= 0);
  }
}