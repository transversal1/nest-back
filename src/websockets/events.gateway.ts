import { Injectable, Logger, UseGuards, Request, Inject } from "@nestjs/common";
import { ConnectedSocket, MessageBody, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer } from "@nestjs/websockets";
import { Server, Socket } from 'socket.io';
import { S3Service } from "src/utils/services/s3.service";
import { NotificationService } from "src/utils/services/notification.service";
import { UsersService } from "src/endpoints/users/user.service";
import { WsGuard } from "src/utils/guards/ws.guard";
import { ContainersService } from "src/endpoints/docker/containers/containers.service";
import { Observable } from "rxjs";
import { cp } from "fs";
import * as stream from "stream";
const ss = require('socket.io-stream')

@Injectable()
@WebSocketGateway(3005, { transports: ['websocket'], cors: false, maxHttpBufferSize: 1e9, pingTimeout: 600000 })
export class EventsGateway implements OnGatewayConnection, OnGatewayDisconnect {
    logger;
    stream : any;
    constructor(
        private readonly userService: UsersService,
        private containerService: ContainersService) {
        this.logger = new Logger(EventsGateway.name);
    }



    handleConnection(client: Socket, ...args: any[]) {
     }



    @WebSocketServer()
    webSocketServer!: Server | undefined;



    @SubscribeMessage('docker.terminal')
    handleInitTerminal(@ConnectedSocket() socket: Socket, @MessageBody() body: any) {
        this.containerService.attach(body.endpoint_id, body.container_id).then(res => {
            this.stream = res[0]
            this.stream.on('data', (d: any) => {
                socket.emit('docker.terminal', d)
            })
            this.stream.write('\n');
        })
    }


    @SubscribeMessage('docker.terminal.command')
    handleNewCommand(@ConnectedSocket() socket: Socket, @MessageBody() body: any) {
        if(this.stream){
            this.stream.write(body.command+'\n');
        }
    }




    handleDisconnect(client: Socket) {
        delete this.stream;
        this.stream = null;
    }
}


