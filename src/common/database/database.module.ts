import { Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { ElasticsearchModule } from "@nestjs/elasticsearch";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Check } from "src/endpoints/checks/entities/check.entity";
import { Endpoint } from "src/endpoints/docker/endpoints/entities/endpoint.entity";
import { Site } from "src/endpoints/sites/entities/site.entity";
import { User } from "src/endpoints/users/entities/user.entity";
import { ApiConfigModule } from "../api-config/api.config.module";
import { ApiConfigService } from "../api-config/api.config.service";

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ApiConfigModule],
      useFactory: (apiConfigService: ApiConfigService) => ({
        name : (Math.random() + 1).toString(36).substring(7),
        type: 'mysql',
        ...apiConfigService.getDatabaseConnection(),
        entities: [User, Site, Check, Endpoint],
        keepConnectionAlive: true,
        synchronize: true,
        
      }),
      inject: [ApiConfigService],
    }),
    TypeOrmModule.forFeature([User, Site, Check,Endpoint]),
    ElasticsearchModule.registerAsync({
      imports : [ApiConfigModule],
      useFactory : (apiConfigService : ApiConfigService) => ({
        node: apiConfigService.getElasticSearchNode(),
        auth: {
          username : "elastic",
          password : "changeme"
        }
      }),
      inject : [ApiConfigService]
    })
  ],
  exports: [
    TypeOrmModule.forFeature([User, Site, Check,Endpoint]),
    ElasticsearchModule
  ],
})
export class DatabaseModule { }